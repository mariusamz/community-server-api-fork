<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Illuminate\Support\Facades\DB;

class VotesTest extends TestCase
{

    public function setUp()
    {
        parent::setUp();
        Artisan::call('migrate:refresh');
    }


    /**
     * A basic functional questions example.
     *
     * @return void
     */
    public function testVotesAndDeleteQuestion()
    {
        $tokenRes = $this->initToken();
        $tokenRes = json_decode($tokenRes);

        $tokenRes2 = $this->initToken(2);
        $tokenRes2 = json_decode($tokenRes2);

        $response = $this->call('POST', '/api/questions', array(
            'text' => 'this is the question',
            'description' => 'this is the description #babum #name #vrum',
            'user' => 'user',
        ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $questionId1 = $responseArr['content']['id'];

        //creation of a question and corresponding tags
        $response = $this->call('POST', '/api/questions', array(
            'text' => 'this is the question',
            'description' => 'this is the description #babum #name #vrum',
            'user' => 'user',
        ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $questionId = $responseArr['content']['id'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $this->assertEquals(201, $response->getStatusCode());

        //adding the first answer for the first question
        $response = $this->call('POST', '/api/questions/' . $questionId1 . '/answers'
            , array(
                'text' => 'this is the first answer'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $this->assertEquals(201, $response->getStatusCode());
        $answer11 = $responseArr['content']['id'];


        //adding the first answer
        $response = $this->call('POST', '/api/questions/' . $questionId . '/answers'
            , array(
                'text' => 'this is the first answer'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );


        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $this->assertEquals(201, $response->getStatusCode());
        $answer1 = $responseArr['content']['id'];

        sleep(1);
        //adding the second answer
        $response = $this->call('POST', '/api/questions/' . $questionId . '/answers'
            , array(
                'text' => 'this is the second answer'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $this->assertEquals(201, $response->getStatusCode());
        $answer2 = $responseArr['content']['id'];

        sleep(1);
        //adding the third answer
        $response = $this->call('POST', '/api/questions/' . $questionId . '/answers'
            , array(
                'text' => 'this is the third answer'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $this->assertEquals(201, $response->getStatusCode());
        $answer3 = $responseArr['content']['id'];


        //List all 3 answers for one question
        $response = $this->call('GET', '/api/questions/' . $questionId . '/answers',
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];

        $this->assertTrue($responseArr['content'][0]->text === 'this is the third answer');
        $this->assertTrue($responseArr['content'][1]->text === 'this is the second answer');
        $this->assertTrue($responseArr['content'][2]->text === 'this is the first answer');

        //adding the first vote up to the answer2
        $response = $this->call('POST', '/api/questions/' . $questionId . '/answers/ ' . $answer2 . '/vote'
            , array(
                'state' => '+1'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );
        $this->assertEquals(201, $response->getStatusCode());

        //List all 3 answers for one question to check for proper sorting
        $response = $this->call('GET', '/api/questions/' . $questionId . '/answers',
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];

        $this->assertTrue($responseArr['content'][0]->text === 'this is the second answer');
        $this->assertTrue($responseArr['content'][0]->votes->hasCurrentUserVoted === true);
        $this->assertTrue($responseArr['content'][0]->votes->count === 1);

        $this->assertTrue($responseArr['content'][1]->text === 'this is the third answer');
        $this->assertTrue($responseArr['content'][1]->votes->hasCurrentUserVoted === false);
        $this->assertTrue($responseArr['content'][1]->votes->count === 0);

        $this->assertTrue($responseArr['content'][2]->text === 'this is the first answer');
        $this->assertTrue($responseArr['content'][2]->votes->hasCurrentUserVoted === false);
        $this->assertTrue($responseArr['content'][2]->votes->count === 0);

        parent::refreshApplication();

        //list all answers by being logged in with different user then the one that voted
        $response = $this->call('GET', '/api/questions/' . $questionId . '/answers',
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes2->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];

        $this->assertTrue($responseArr['content'][0]->text === 'this is the second answer');
        $this->assertTrue($responseArr['content'][0]->votes->hasCurrentUserVoted === false);
        $this->assertTrue($responseArr['content'][0]->votes->count === 1);

        $this->assertTrue($responseArr['content'][1]->text === 'this is the third answer');
        $this->assertTrue($responseArr['content'][1]->votes->hasCurrentUserVoted === false);
        $this->assertTrue($responseArr['content'][1]->votes->count === 0);

        $this->assertTrue($responseArr['content'][2]->text === 'this is the first answer');
        $this->assertTrue($responseArr['content'][2]->votes->hasCurrentUserVoted === false);
        $this->assertTrue($responseArr['content'][2]->votes->count === 0);

        sleep(1);
        //adding the first vote up to $answer1
        $response = $this->call('POST', '/api/questions/' . $questionId . '/answers/ ' . $answer1 . '/vote'
            , array(
                'state' => '+1'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes2->token]
        );
        $this->assertEquals(201, $response->getStatusCode());

        //list all answers by being logged in with $tokenRes2 the user that just voted up
        $response = $this->call('GET', '/api/questions/' . $questionId . '/answers',
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes2->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];

        $this->assertTrue($responseArr['content'][0]->text === 'this is the second answer');
        $this->assertTrue($responseArr['content'][0]->votes->hasCurrentUserVoted === false);
        $this->assertTrue($responseArr['content'][0]->votes->count === 1);

        $this->assertTrue($responseArr['content'][1]->text === 'this is the first answer');
        $this->assertTrue($responseArr['content'][1]->votes->hasCurrentUserVoted === true);
        $this->assertTrue($responseArr['content'][1]->votes->count === 1);

        $this->assertTrue($responseArr['content'][2]->text === 'this is the third answer');
        $this->assertTrue($responseArr['content'][2]->votes->hasCurrentUserVoted === false);
        $this->assertTrue($responseArr['content'][2]->votes->count === 0);


        //test that i can not vote up twice
        $response = $this->call('POST', '/api/questions/' . $questionId . '/answers/ ' . $answer1 . '/vote'
            , array(
                'state' => '+1'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes2->token]
        );
        $this->assertEquals(422, $response->getStatusCode());


        //test unvote for an answer that the user did not vote for and must be rejected
        $response = $this->call('POST', '/api/questions/' . $questionId . '/answers/ ' . $answer2 . '/vote'
            , array(
                'state' => '0'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes2->token]
        );
        $this->assertEquals(422, $response->getStatusCode());

        //test unvote for an answer that the user has voted for
        $response = $this->call('POST', '/api/questions/' . $questionId . '/answers/ ' . $answer1 . '/vote'
            , array(
                'state' => '0'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes2->token]
        );
        $this->assertEquals(201, $response->getStatusCode());

        //list all answers by being logged in with $tokenRes2 the user that just unvoted
        $response = $this->call('GET', '/api/questions/' . $questionId . '/answers',
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes2->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];

        $this->assertTrue($responseArr['content'][0]->text === 'this is the second answer');
        $this->assertTrue($responseArr['content'][0]->votes->hasCurrentUserVoted === false);
        $this->assertTrue($responseArr['content'][0]->votes->count === 1);

        $this->assertTrue($responseArr['content'][1]->text === 'this is the third answer');
        $this->assertTrue($responseArr['content'][1]->votes->hasCurrentUserVoted === false);
        $this->assertTrue($responseArr['content'][1]->votes->count === 0);

        $this->assertTrue($responseArr['content'][2]->text === 'this is the first answer');
        $this->assertTrue($responseArr['content'][2]->votes->hasCurrentUserVoted === false);
        $this->assertTrue($responseArr['content'][2]->votes->count === 0);

        //add vote for the initial question
        $response = $this->call('POST', '/api/questions/' . $questionId1 . '/answers/ ' . $answer11 . '/vote'
            , array(
                'state' => '+1'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes2->token]
        );
        $this->assertEquals(201, $response->getStatusCode());


        //add more votes
        $response = $this->call('POST', '/api/questions/' . $questionId . '/answers/ ' . $answer2 . '/vote'
            , array(
                'state' => '+1'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes2->token]
        );
        $this->assertEquals(201, $response->getStatusCode());

        //add more votes
        $response = $this->call('POST', '/api/questions/' . $questionId . '/answers/ ' . $answer3 . '/vote'
            , array(
                'state' => '+1'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes2->token]
        );
        $this->assertEquals(201, $response->getStatusCode());


        //try to delete a question if use is not mentor
        $response = $this->call('DELETE', '/api/questions/' . $questionId,
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes2->token]
        );

        $this->assertEquals(400, $response->getStatusCode());


        //delete $questionId
        $response = $this->call('DELETE', '/api/questions/' . $questionId,
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $this->assertEquals(200, $response->getStatusCode());


        //try to get $questionId that was just deleted
        $response = $this->call('GET',
            '/api/questions/' . $questionId,
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $this->assertEquals(404, $response->getStatusCode());

        //try to get $questionId1
        $response = $this->call('GET',
            '/api/questions/' . $questionId1,
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $this->assertEquals(200, $response->getStatusCode());

    }
}