<?php

use Illuminate\Support\Facades\Artisan;

class ProfileTest extends TestCase
{

    public function setUp()
    {
        parent::setUp();
        Artisan::call('migrate:refresh');
    }


    /**
     * A basic functional questions example.
     *
     * @return void
     */
    public function testProfileResponse()
    {
        $tokenRes = $this->initToken();
        $tokenRes = json_decode($tokenRes);

        //get private profile
        $response = $this->call('GET', '/api/user/marius/profile',
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue($responseArr['content']['username'] === 'marius');
        $this->assertTrue(isset($responseArr['content']['name']));
        $this->assertTrue(isset($responseArr['content']['biography']));
        $this->assertTrue(isset($responseArr['content']['email']));
        $this->assertTrue(isset($responseArr['content']['location']));
        $this->assertTrue(isset($responseArr['content']['dateJoined']));
        $this->assertTrue(isset($responseArr['content']['avatar']) === true);
        $this->assertTrue($responseArr['content']['instructor'] === true);
        $this->assertTrue($responseArr['content']['mentor'] === true);

        //get public profile
        $response = $this->call('GET', '/api/user/chriss/profile',
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue($responseArr['content']['username'] === 'chriss');
        $this->assertTrue(isset($responseArr['content']['name']));
        $this->assertTrue(isset($responseArr['content']['biography']));
        $this->assertTrue(isset($responseArr['content']['location']));
        $this->assertTrue(isset($responseArr['content']['dateJoined']));
        $this->assertTrue(isset($responseArr['content']['avatar']) === true);
        $this->assertTrue($responseArr['content']['instructor'] === false);
        $this->assertTrue($responseArr['content']['mentor'] === false);

        $this->assertTrue(!isset($responseArr['content']['email']));


    }
}