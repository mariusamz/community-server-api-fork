<?php

use Illuminate\Foundation\Testing\WithoutMiddleware;
use Illuminate\Foundation\Testing\DatabaseMigrations;
use Illuminate\Foundation\Testing\DatabaseTransactions;
use Illuminate\Support\Facades\Artisan;
use Tymon\JWTAuth\JWTManager as JWT;


class QuestionsTest extends TestCase
{

    public function setUp()
    {
        parent::setUp();
        Artisan::call('migrate:refresh');
    }

    /**
     * A basic functional questions creation and listing.
     *
     * @return void
     */
    public function testQuestionCreationListFilter()
    {
        $tokenRes = $this->initToken();
        $tokenRes = json_decode($tokenRes);

        DB::connection()->getPdo()->exec("UPDATE members SET avatar = 'testimg.jpg' Where usid = 185094 LIMIT 1");

        //Check adding answers to a question without tags and verify proper response is received
        $response = $this->call('POST', '/api/questions', [
            'text'        => 'this is the question',
            'description' => 'this is the description',
            'courseTag'   => '#courseTag',
            'lessonId'    => '2'
        ],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $questionIds[0] = $responseArr['content']['id'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $this->assertEquals(201, $response->getStatusCode());

        //list the 1 previously created question
        $response = $this->call('GET',
            '/api/questions/' . $questionIds[0] ,
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];

        $this->assertTrue(isset($responseArr['content']['id']));
        $this->assertTrue(isset($responseArr['content']['text']));
        $this->assertTrue(isset($responseArr['content']['detail']));
        $this->assertTrue(isset($responseArr['content']['user']));
        $this->assertTrue(isset($responseArr['content']['hasAcceptedAnswer']));

        $this->assertTrue(isset($responseArr['content']['courseId']) && !empty($responseArr['content']['courseId']));
        $this->assertTrue(isset($responseArr['content']['lessonTitle']) && !empty($responseArr['content']['lessonTitle']));
        $this->assertTrue(isset($responseArr['content']['lessonId']) && !empty($responseArr['content']['lessonId']));

        $this->assertTrue(isset($responseArr['content']['created_at']));
        $this->assertTrue(isset($responseArr['content']['updated_at']));


        //list all the previously created question
        $response = $this->call('GET',
            '/api/questions' ,
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $response = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$response['content'];
        $this->assertTrue($responseArr['content'][0]->detail === 'this is the description #courseTag');

        $questions = DB::select("SELECT * FROM forum_questions LIMIT 1");

        $this->assertTrue($questions[0]->lesson_id === 2);

        sleep(1);
        //Check creating a question with tags and verify proper response is received
        $response = $this->call('POST', '/api/questions', array(
            'text' => 'this is the question',
            'description' => 'this is the description #babum #name #vrum',
            
        ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $questionIds[1] = $responseArr['content']['id'];
        $this->assertEquals(201, $response->getStatusCode());

        //list the question previously created
        $response = $this->call('GET',
            '/api/questions/' . $responseArr['content']['id'],
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );


        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue(isset($responseArr['server_date']) && !empty($responseArr['server_date']));
        $this->assertTrue(count($responseArr['content']['tags']) === 3);
        $this->assertTrue($responseArr['content']['tags'][0]->name === 'babum');

        $this->assertTrue(isset($responseArr['content']['text']));
        $this->assertTrue(isset($responseArr['content']['detail']));

        $this->assertTrue($responseArr['content']['hasAcceptedAnswer'] === 0);
        $this->assertTrue($responseArr['content']['numberOfAnswers'] === 0);

        $this->assertTrue(is_object($responseArr['content']['user']));
        $this->assertTrue(!empty($responseArr['content']['user']->id));
        $this->assertTrue(!empty($responseArr['content']['user']->username));
        $this->assertTrue(!empty($responseArr['content']['user']->avatarUrl));
        $this->assertTrue(strpos($responseArr['content']['user']->avatarUrl, env('AWS_S3_ENDPOINT')) === 0);
        $this->assertTrue(!empty($responseArr['content']['user']->isMentor));


        sleep(1);
        //Check creating a question without tags and verify proper response is received
        $response = $this->call('POST', '/api/questions', array(
            'text' => 'this is the question',
            'description' => 'this is the description',
            
        ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $questionIds[2] = $responseArr['content']['id'];
        $this->assertEquals(201, $response->getStatusCode());

        //list all 3 previously created questions
        $response = $this->call('GET',
            '/api/questions' ,
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr1 = (array)json_decode($response->getContent());
        $responseArr1['content'] = (array)$responseArr1['content'];
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue(count($responseArr1['content']) === 3);

        $this->assertTrue(isset($responseArr1['content'][0]->id));
        $this->assertTrue(isset($responseArr1['content'][0]->text));
        $this->assertTrue(isset($responseArr1['content'][0]->detail));
        $this->assertTrue(isset($responseArr1['content'][0]->user));
        $this->assertTrue(isset($responseArr1['content'][0]->hasAcceptedAnswer));
        $this->assertTrue(isset($responseArr1['content'][0]->created_at));
        $this->assertTrue(isset($responseArr1['content'][0]->updated_at));


        //adding the first 2 answers for one question
        for($i=0; $i<2; $i++) {
            $response = $this->call('POST', '/api/questions/' . $responseArr1['content'][0]->id . '/answers'
                , array(
                    'text' => 'this is the first answer '.$i
                ),
                [/* cookies */],
                [/* files */],
                ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
            );

            $this->assertEquals(201, $response->getStatusCode());
        }


        //list the question previously created
        $response = $this->call('GET',
            '/api/questions/' . $responseArr1['content'][0]->id,
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );


        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue(isset($responseArr['server_date']) && !empty($responseArr['server_date']));
        $this->assertTrue($responseArr['content']['numberOfAnswers'] === 2);

        //list only 2 questions as the first one has an answer
        $response = $this->call('GET',
            '/api/questions?mode=unanswered' ,
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue(count($responseArr['content']) === 2);

        $this->assertTrue(isset($responseArr['content'][0]->numberOfAnswers));

        //check that sorting is oldest at the top
        $this->assertTrue(isset($responseArr['content'][0]->id)
            && $responseArr['content'][0]->id == $questionIds[1]);
        $this->assertTrue(isset($responseArr['content'][1]->id)
            && $responseArr['content'][1]->id == $questionIds[2]);

        //adding the first answer
        $response = $this->call('POST', '/api/questions/' . $responseArr1['content'][1]->id . '/answers'
            , array(
                'text' => 'this is the first answer'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $this->assertEquals(201, $response->getStatusCode());

        //list only 1 questions as the first one has an answer
        $response = $this->call('GET',
            '/api/questions?mode=unanswered' ,
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue(count($responseArr['content']) === 1);
    }


    /**
     * Check scenarios for question with answers and tags
     */
    public function testAnswersCreationListAndSolve()
    {
        $tokenRes = $this->initToken();
        $tokenRes = json_decode($tokenRes);

        $tokenRes2 = $this->initToken(2);
        $tokenRes2 = json_decode($tokenRes2);

        //Question creation call
        $response = $this->call('POST', '/api/questions', array(
            'text' => 'this is the question',
            'description' => 'this is the description',
            
        ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $questionId = $responseArr['content']['id'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $this->assertEquals(201, $response->getStatusCode());

        //try to list a non existent answer from an existing question
        $response = $this->call('GET', '/api/questions/' . $questionId . '/answers/1111',
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );
        $this->assertEquals(404, $response->getStatusCode());

        //adding the first answer
        $response = $this->call('POST', '/api/questions/' . $questionId . '/answers'
            , array(
                'text' => 'this is the first answer'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $this->assertEquals(201, $response->getStatusCode());
        $questionId1 = $responseArr['content']['id'];

        sleep(1);

        //adding 2 answer
        $response = $this->call('POST', '/api/questions/' . $questionId . '/answers'
            , array(
                'text' => 'this is the #tag1 answer #tag2 #tag3'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );
        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $this->assertEquals(201, $response->getStatusCode());

        //List all answers for one question
        $response = $this->call('GET', '/api/questions/' . $questionId . '/answers',
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];

        $this->assertTrue(isset($responseArr['content'][0]->created_at));
        $this->assertTrue(isset($responseArr['content'][0]->updated_at));
        $this->assertTrue(is_object($responseArr['content'][0]->user));

        $this->assertTrue($responseArr['content'][1]->text === 'this is the first answer');
        $this->assertTrue(isset($responseArr['content'][1]->created_at));
        $this->assertTrue(isset($responseArr['content'][1]->updated_at));
        $this->assertTrue(is_object($responseArr['content'][1]->user));



        //list one answer given by Id and Answer Id
        $response = $this->call('GET', '/api/questions/' . $questionId . '/answers/' . $questionId1,
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue($responseArr['content']['text'] === 'this is the first answer');
        $this->assertTrue(isset($responseArr['server_date']) && !empty($responseArr['server_date']));
        $this->assertEquals(200, $response->getStatusCode());

        //try to add an answer to a question that does not exist
        $response = $this->call('POST', '/api/questions/0/answers'
            , array(
                'text' => 'this is the #tag1 answer #tag2 #tag3'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );
        $this->assertEquals(400, $response->getStatusCode());


        parent::refreshApplication();

        //try to solve a question by selecting a non existent answer
        $response = $this->call('POST', '/api/questions/1/answers/11/accept'
            , array(
                'state' => '1'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );
        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals('Answer validation error', $response->getContent());

        parent::refreshApplication();

        //try to solve a question by using a different user as the one who owns the question
        $response = $this->call('POST', '/api/questions/1/answers/1/accept'
            , array(
                'state' => '1'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes2->token]
        );
        $this->assertEquals(422, $response->getStatusCode());

        $this->assertEquals('Question validation error', $response->getContent());

        parent::refreshApplication();

        //try to solve a non existent question
        $response = $this->call('POST', '/api/questions/100/answers/1/accept'
            , array(
                'state' => '1'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );
        $this->assertEquals(422, $response->getStatusCode());

        $this->assertEquals('Question validation error', $response->getContent());


        //try to solve a question using a wrong state parameter
        $response = $this->call('POST', '/api/questions/1/answers/2/accept'
            , array(
                'state' => '10'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals('State is incorect', $response->getContent());

        parent::refreshApplication();
        //get the question details with the owner of the question as logged in before solved the success call
        $response = $this->call('GET', '/api/questions/' . $questionId,
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue($responseArr['content']['hasAcceptedAnswer'] === 0);
        $this->assertTrue($responseArr['content']['currentUserCanAcceptSolution'] === 1);

        parent::refreshApplication();

        //get the question details while not logged in with the owner of the question before solved the success call
        $response = $this->call('GET', '/api/questions/' . $questionId,
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes2->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue($responseArr['content']['hasAcceptedAnswer'] === 0);
        $this->assertTrue($responseArr['content']['currentUserCanAcceptSolution'] === 0);

        parent::refreshApplication();

        //try to solve a question with success
        $response = $this->call('POST', '/api/questions/1/answers/1/accept'
            , array(
                'state' => '1'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );
        $this->assertEquals(201, $response->getStatusCode());

        //get the question details after solved the success call
        $response = $this->call('GET', '/api/questions/' . $questionId,
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue($responseArr['content']['hasAcceptedAnswer'] === 1);
        $this->assertTrue($responseArr['content']['currentUserCanAcceptSolution'] === 0);


        parent::refreshApplication();

        //get the question details after solved the success call
        $response = $this->call('GET', '/api/questions/' . $questionId,
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes2->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue($responseArr['content']['hasAcceptedAnswer'] === 1);
        $this->assertTrue($responseArr['content']['currentUserCanAcceptSolution'] === 0);


        //try to solve the same question and answer as previous call and fail
        $response = $this->call('POST', '/api/questions/1/answers/1/accept'
            , array(
                'state' => '1'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $this->assertEquals(422, $response->getStatusCode());

        //try to have two answers selected as solved for the same question
        $response = $this->call('POST', '/api/questions/1/answers/2/accept'
            , array(
                'state' => '1'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $this->assertEquals(422, $response->getStatusCode());
        $this->assertEquals('Question validation error', $response->getContent());

        sleep(1);
        //adding 3rd answer
        $response = $this->call('POST', '/api/questions/' . $questionId . '/answers'
            , array(
                'text' => 'this is the 3 answer'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $this->assertEquals(201, $response->getStatusCode());
        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $questionId3 = $responseArr['content']['id'];


        //List all answers for one question
        $response = $this->call('GET', '/api/questions/' . $questionId . '/answers',
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];

        $this->assertTrue($responseArr['content'][0]->text === 'this is the first answer');
        $this->assertTrue($responseArr['content'][0]->accepted === 1);
        $this->assertTrue($responseArr['content'][1]->text === 'this is the 3 answer');
        $this->assertTrue($responseArr['content'][2]->accepted === 0);
        $this->assertTrue($responseArr['content'][2]->text === 'this is the #tag1 answer #tag2 #tag3');
        $this->assertTrue($responseArr['content'][2]->accepted === 0);
    }

    /**
     * Test the way the question modes behave
     */
    public function testQuestionModes(){
        $tokenRes = $this->initToken();
        $tokenRes = json_decode($tokenRes);

        $response = $this->call('POST', '/api/questions', array(
            'text' => 'this is the question #tagger1 #tagger2',
            'description' => 'this is the description',

        ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr1 = (array)json_decode($response->getContent());

        sleep(1);
        $response = $this->call('POST', '/api/questions', array(
            'text' => 'this is the question',
            'description' => 'this is the description',

        ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr2 = (array)json_decode($response->getContent());

        sleep(1);
        $response = $this->call('POST', '/api/questions', array(
            'text' => 'this is the question',
            'description' => 'this is the description',

        ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr3 = (array)json_decode($response->getContent());

        //list the active questions
        $response = $this->call('GET',
            '/api/questions?mode=active' ,
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue(count($responseArr['content']) === 0);

        sleep(1);
        $response = $this->call('POST', '/api/questions/' . $responseArr1['content']->id . '/answers'
            , array(
                'text' => 'this is the first answer'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $this->assertEquals(201, $response->getStatusCode());
        $answer1 = $responseArr['content']['id'];


        //list the active questions
        $response = $this->call('GET',
            '/api/questions?mode=active' ,
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue(count($responseArr['content']) === 1);
        $this->assertTrue(isset($responseArr['content'][0]->numberOfAnswers));
        $this->assertTrue($responseArr['content'][0]->id == $responseArr1['content']->id);
        $this->assertTrue(count($responseArr['content'][0]->tags) === 2);

        sleep(1);
        //add another question
        $response = $this->call('POST', '/api/questions', array(
            'text' => 'this is the question',
            'description' => 'this is the description',

        ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );
        $this->assertEquals(201, $response->getStatusCode());
        $responseArr4 = (array)json_decode($response->getContent());


        sleep(1);
        //adding the second answer
        $response = $this->call('POST', '/api/questions/' . $responseArr2['content']->id . '/answers'
            , array(
                'text' => 'this is the first answer'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );
        $this->assertEquals(201, $response->getStatusCode());
        $responseArrAnswer2 = (array)json_decode($response->getContent());

        //list the active questions
        $response = $this->call('GET',
            '/api/questions?mode=active' ,
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue(count($responseArr['content']) === 2);


        $this->assertTrue($responseArr['content'][0]->id == $responseArr2['content']->id);
        $this->assertTrue($responseArr['content'][1]->id == $responseArr1['content']->id);

        sleep(1);
        //adding the first vote up to the $responseArr1
        $response = $this->call('POST', '/api/questions/' . $responseArr1['content']->id . '/answers/ ' .
            $responseArrAnswer2['content']->id . '/vote'
            , array(
                'state' => '+1'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );
        $this->assertEquals(201, $response->getStatusCode());

        sleep(1);
        //try to solve a question with success
        $response = $this->call('POST', '/api/questions/'. $responseArr2['content']->id .'/answers/'.
            $responseArrAnswer2['content']->id . '/accept',
            array(
                'state' => '1'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );
        $this->assertEquals(201, $response->getStatusCode());

        //list the active questions
        $response = $this->call('GET',
            '/api/questions?mode=active' ,
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue(count($responseArr['content']) === 2);

        $this->assertTrue($responseArr['content'][0]->id == $responseArr2['content']->id);
        $this->assertTrue($responseArr['content'][1]->id == $responseArr1['content']->id);

    }


    public function testFilteredQuestions()
    {
        $tokenRes = $this->initToken();
        $tokenRes = json_decode($tokenRes);

        $tokenRes2 = $this->initToken(2);
        $tokenRes2 = json_decode($tokenRes2);

        $tokenRes3 = $this->initToken(3);
        $tokenRes3 = json_decode($tokenRes3);


        $response = $this->call('POST', '/api/questions', array(
            'text' => 'this is the question',
            'description' => 'this is the description',

        ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertEquals(201, $response->getStatusCode());

        parent::refreshApplication();

        $response = $this->call('POST', '/api/questions', array(
            'text' => 'this is the 2 question',
            'description' => 'this is the description 2',

        ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes2->token]
        );

        $responseArr2 = (array)json_decode($response->getContent());
        $responseArr2['content'] = (array)$responseArr2['content'];
        $this->assertEquals(201, $response->getStatusCode());


        parent::refreshApplication();

        $response = $this->call('GET',
            '/api/questions?mode=user&username=marius',
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue(count($responseArr['content']) === 1);

        $this->assertTrue($responseArr['content'][0]->text === 'this is the question');


        parent::refreshApplication();

        $response = $this->call('GET',
            '/api/questions?mode=user&username=chriss',
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes2->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue(count($responseArr['content']) === 1);

        $this->assertTrue($responseArr['content'][0]->text === 'this is the 2 question');

        $response = $this->call('POST', '/api/questions', [
            'text'        => 'this is the question 3',
            'description' => 'this is the description 3',
            'courseTag'   => '#courseTag',
            'lessonId'    => '2'
        ],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $questionIds[0] = $responseArr['content']['id'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $this->assertEquals(201, $response->getStatusCode());


        $response = $this->call('POST', '/api/questions/audience/ASM7', [
            'text'        => 'this is the question ASM7 3',
            'description' => 'this is the description ASM7 3',
            'courseTag'   => '#courseTag',
            'lessonId'    => '2'
        ],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $questionIdsAsm[0] = $responseArr['content']['id'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $this->assertEquals(201, $response->getStatusCode());

        parent::refreshApplication();

        $response = $this->call('GET',
            '/api/questions/coursetag/courseTag',
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes2->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue(count($responseArr['content']) === 1);
        $this->assertTrue($responseArr['content'][0]->text === 'this is the question 3');

        parent::refreshApplication();

        $response = $this->call('GET',
            '/api/questions/lessonid/2',
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes2->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue(count($responseArr['content']) === 1);
        $this->assertTrue($responseArr['content'][0]->text === 'this is the question 3');
        $this->assertTrue($responseArr['content'][0]->tags[0]->name === 'coursetag');


        //check to see that we can access only the content flaged for ASM7
        $response = $this->call('GET',
            '/api/questions/lessonid/2/audience/ASM7',
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue(count($responseArr['content']) === 1);
        $this->assertTrue($responseArr['content'][0]->text === 'this is the question ASM7 3');
        $this->assertTrue($responseArr['content'][0]->tags[0]->name === 'coursetag');


        //check that we get an error when we try to access questions for ASM7 with a token that does not have ASM7 creed
        $response = $this->call('GET',
            '/api/questions/lessonid/2/audience/ASM7',
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes3->token]
        );

        $this->assertEquals(403, $response->getStatusCode());
        $this->assertEquals("No rights to see this content", $response->getData());
    }


    /**
     * A basic functional testing of questions edit and delete.
     *
     * @return void
     */
    public function testQuestionEditDelete()
    {
        $tokenRes = $this->initToken();
        $tokenRes = json_decode($tokenRes);

        $tokenRes2 = $this->initToken(2);
        $tokenRes2 = json_decode($tokenRes2);

        //Create question
        $response = $this->call('POST', '/api/questions', [
            'text'        => 'this is the #tag1 question',
            'description' => 'this is the #tag2 #tag3 description',
            'courseTag'   => '#courseTag',
            'lessonId'    => '2'
        ],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes2->token]
        );

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $questionIds[0] = $responseArr['content']['id'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $this->assertEquals(201, $response->getStatusCode());


        sleep(1);
        //Check 2 question
        $response = $this->call('POST', '/api/questions', array(
            'text' => 'this is the question',
            'description' => 'this is the description #babum #name #vrum',

        ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $questionIds[1] = $responseArr['content']['id'];
        $this->assertEquals(201, $response->getStatusCode());

        sleep(1);
        //Check 3rd question
        $response = $this->call('POST', '/api/questions', array(
            'text' => 'this is the question',
            'description' => 'this is the description',

        ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $questionIds[2] = $responseArr['content']['id'];
        $this->assertEquals(201, $response->getStatusCode());

        //list all 3 previously created questions
        $response = $this->call('GET',
            '/api/questions' ,
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr1 = (array)json_decode($response->getContent());
        $responseArr1['content'] = (array)$responseArr1['content'];
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue(count($responseArr1['content']) === 3);


        //Edit first question with user as not an mentor
        $response = $this->call('PATCH', '/api/questions/' . $questionIds[0], [
            'text'        => 'this is the edited #tagedit1 question',
            'description' => 'this is the edited #tagedit2 description',
            'courseTag'   => '#courseTag',
            'lessonId'    => '2'
        ],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes2->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $this->assertEquals(400, $response->getStatusCode());

        //Edit first question with user as mentor
        $response = $this->call('PATCH', '/api/questions/' . $questionIds[0], [
            'text'        => 'this is the edited #tagedit1 question',
            'description' => 'this is the edited #tagedit2 description',
            'courseTag'   => '#courseTag',
            'lessonId'    => '2'
        ],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $this->assertEquals(200, $response->getStatusCode());

        sleep(1);
        //list the question previously created and check the edited values are present
        $response = $this->call('GET',
            '/api/questions/' . $questionIds[0],
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );


        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue(isset($responseArr['server_date']) && !empty($responseArr['server_date']));
        $this->assertTrue($responseArr['content']['text'] === 'this is the edited #tagedit1 question');
        $this->assertTrue($responseArr['content']['detail'] === 'this is the edited #tagedit2 description #courseTag' );


        //Try to Delete first question with user as not an mentor
        $response = $this->call('DELETE', '/api/questions/' . $questionIds[0], [
            'text'        => 'this is the edited #tagedit1 question',
            'description' => 'this is the edited #tagedit2 description',
            'courseTag'   => '#courseTag',
            'lessonId'    => '2'
        ],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes2->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $this->assertEquals(400, $response->getStatusCode());
        $this->assertTrue($response->getData() === 'Validation error, not a mentor');


        //Try to Delete first question with user as a mentor
        $response = $this->call('DELETE', '/api/questions/' . $questionIds[0], [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $this->assertEquals(200, $response->getStatusCode());


        $response = $this->call('GET', '/api/questions/' . $questionIds[0], [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $this->assertEquals(404, $response->getStatusCode());

    }


    /**
     * A basic functional testing of answers edit.
     *
     * @return void
     */
    public function testAnswerEdit()
    {
        $tokenRes = $this->initToken();
        $tokenRes = json_decode($tokenRes);

        $tokenRes2 = $this->initToken(2);
        $tokenRes2 = json_decode($tokenRes2);

        //Create question
        $response = $this->call('POST', '/api/questions', [
            'text'        => 'this is the #tag1 question',
            'description' => 'this is the #tag2 #tag3 description',
            'courseTag'   => '#courseTag',
            'lessonId'    => '2'
        ],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes2->token]
        );

        $responseArr = (array)json_decode($response->getContent());

        $responseArr['content'] = (array)$responseArr['content'];
        $questionIds[0] = $responseArr['content']['id'];
        $this->assertTrue(isset($responseArr['content']['id']));
        $this->assertEquals(201, $response->getStatusCode());

        sleep(1);

        $response = $this->call('POST', '/api/questions/' . $questionIds[0] . '/answers'
            , array(
                'text' => 'this is the first answer 1 #tag1 #tag2'
            ),
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $this->assertEquals(201, $response->getStatusCode());
        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $answerId[0] = $responseArr['content']['id'];

        //list the answer previously created
        $response = $this->call('GET',
            '/api/questions/' . $questionIds[0]. '/answers',
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertEquals(200, $response->getStatusCode());


        //Edit first answer with user as not an mentor
        $response = $this->call('PATCH', '/api/questions/' . $questionIds[0] . '/answers/' . $answerId[0], [
            'text'        => 'this is the first answer 1 #tag1 #tag2 edited'
        ],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes2->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $this->assertEquals(400, $response->getStatusCode());

        //Edit first answer with user as not an mentor
        $response = $this->call('PATCH', '/api/questions/' . $questionIds[0] . '/answers/' . $answerId[0], [
            'text'        => 'this is the first answer 1 #tag1 #tag2 edited'
        ],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $this->assertEquals(200, $response->getStatusCode());


        //list the answer previously created and check the edited values are present
        $response = $this->call('GET',
            '/api/questions/' . $questionIds[0]. '/answers',
            [],
            [/* cookies */],
            [/* files */],
            ['HTTP_Authorization' => 'Bearer ' . $tokenRes->token]
        );

        $responseArr = (array)json_decode($response->getContent());
        $responseArr['content'] = (array)$responseArr['content'];
        $this->assertEquals(200, $response->getStatusCode());
        $this->assertTrue(isset($responseArr['server_date']) && !empty($responseArr['server_date']));
        $this->assertTrue($responseArr['content'][0]->text === 'this is the first answer 1 #tag1 #tag2 edited');

    }
}