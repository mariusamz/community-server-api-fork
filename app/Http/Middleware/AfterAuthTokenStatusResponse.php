<?php namespace App\Http\Middleware;

use Amazing\Services\Auth\AuthService;

use Closure;
use Illuminate\Http\JsonResponse;
use Illuminate\Http\Request;
use Tymon\JWTAuth\Exceptions\JWTException;
use Tymon\JWTAuth\Exceptions\TokenExpiredException;


class AfterAuthTokenStatusResponse
{
    private $auth_service;

    public function __construct( AuthService $auth_service )
    {
        $this->auth_service = $auth_service;
    }


    public function handle( $request, Closure $next)
    {
        $response = $next($request);

        if( $response instanceof JsonResponse /* || $response->headers->get('content-type') == 'application/json' */)
        {
            if( $request->hasHeader( 'Authorization' ) || $request->has( 'token' ) )
            {
                $collection = $response->getData( true );

                if( !str_contains( $request->path(), "refresh-token" ) && is_array($collection) )
                {
                    $collection['auth'] = ['status' => 'token_valid'];
                    try {
                        $member_id = $this->auth_service->getMemberIdWithException();
                        $collection['auth']['member_id'] = $member_id;
                    } catch (TokenExpiredException $e) {
                        $collection['auth']['status'] = 'token_expired';
                    } catch (JWTException $e) {
                        $collection['auth']['status'] = 'token_invalid';
                    }
                }
                $response->setData($collection);

            }
        }

        return $response;
    }
}



