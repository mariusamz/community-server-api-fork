<?php
/**
 * Created by PhpStorm.
 * User: marius
 * Date: 01/09/2016
 * Time: 11:35
 */

namespace App\Http\Controllers\Profile;
use Amazing\Model\Member;

class PrivateProfileVIew
{
    public $avatar;
    public $username;
    public $name;
    public $biography;
    public $email;
    public $location;
    public $dateJoined;
    public $instructor;
    public $mentor;


    /**
     * PrivateProfileVIew constructor.
     * @param $member Member
     */
    public function __construct(Member $member)
    {
        $this->avatar = $member->avatar;
        $this->username = $member->username;
        $this->name = $member->firstname . ' ' . $member->lastname;
        $this->biography = $member->bio;
        $this->email = $member->email;
        $this->location = $member->location;
        $this->dateJoined = $member->dateadded->format('M/d/y');
        $this->instructor = $member->is_instructor;
        $this->mentor = $member->is_mentor;
    }
}