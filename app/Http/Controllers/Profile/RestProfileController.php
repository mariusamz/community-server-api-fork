<?php
/**
 * Created by PhpStorm.
 * User: marius
 * Date: 06/01/2017
 * Time: 12:12
 */

namespace App\Http\Controllers\Profile;

use Amazing\Model\Member;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Log;
use Amazing\Services\Auth\AuthService;


class RestProfileController extends BaseController
{
    private $authService;

    function __construct(AuthService $authService)
    {
        $this->authService = $authService;
    }

    /**
     * Return dedicated question types.
     *
     * @param \App\Http\Controllers\Profile\Request|Request $request
     * @return JsonResponse
     */
    function getProfile(Request $request)
    {
        Log::debug("Request getProfile started " . microtime());

        if (is_null($request->route('username'))) {
            return response()->json('Validation error, missing username', 400);
        }

        Log::debug("Request getProfile started " . $request->route('username'));

        /** @var $member Member */
        $member = Member::with('instructorProfiles')->where('username', $request->route('username'))->first();

        if ($member == null) {
            return response()->json('User not found', 404);
        }

        $member->is_instructor = false;
        if (!empty($member->instructorProfiles->toArray())) {
            $member->is_instructor = true;
        }

        if ($member->is_mentor === 0) {
            $member->is_mentor = false;
        } else {
            $member->is_mentor = true;
        }

        if(!empty($member->avatar)) {
            $avatarUrl = implode('/', [
                env('AWS_S3_ENDPOINT'),
                env('AWS_S3_BUCKET_MEMBER_AVATARS'),
                $member->avatar
            ]);

            $member->avatar = $avatarUrl;
        }

        $loggedInMember = $this->authService->getAuthenticatedUser();
        if ($loggedInMember == null) {
            return response()->json('Authentication error', 403);
        }

        if($member->username === $loggedInMember->username){
            $response = new PrivateProfileVIew($member);
        } else {
            $response = new PublicProfileVIew($member);
        }

        Log::debug("Request getProfile ended " . microtime());

        return response()->json([
                'content' => $response,
                'server_date' => strtotime('now')
            ]
        );
    }
}