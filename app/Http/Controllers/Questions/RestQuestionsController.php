<?php

namespace App\Http\Controllers\Questions;

use Amazing\Model\Answer;
use Amazing\Model\AnswerVote;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Amazing\Model\Question;
use Amazing\Model\Tag;
use Amazing\Model\Member;
use Illuminate\Support\Facades\Log;
use Amazing\Services\Auth\AuthService;
use Illuminate\Support\Facades\Auth;
use Amazing\Services\MemberService;


class RestQuestionsController extends BaseController
{
    const QUESTIONSPERPAGE = 50;
    private $authService;
    private $memberService;

    function __construct(AuthService $authService, MemberService $memberService)
    {
        $this->authService = $authService;
        $this->memberService = $memberService;
    }

    /**
     * Get a single question identified by given id
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function getQuestion(Request $request)
    {
        if (is_null($request->route('question_id'))) {
            return response()->json('Validation error', 400);
        }

        $filterByAudience = '';
        if (!is_null($request->route('audience'))) {

            $response = $this->authService->validateRequestOrigin(
                $request->route('audience')
            );

            $filterByAudience = $request->route('audience');

            if($response !== true){
                return response()->json('No rights to see this content', 403);
            }
        }

        Log::debug("Request question started " . $request->route('question_id'));

        $question = Question::with(['member'])
            ->leftjoin('course_lessons as cl', 'cl.lid', '=', 'forum_questions.lesson_id')
            ->leftjoin('course_modules as cm', 'cm.mid', '=', 'cl.mid')
            ->where('audience', $filterByAudience)
            ->where('id', $request->route('question_id'))
            ->first(['forum_questions.*', 'cl.title', 'cm.cid']);

        if (empty($question)) {
            return response()->make('Not found question', 404);
        }

        $questionArr = $question->toArray();
        $questionArr['user'] = $question->member;

        $questionArr['objTags'] = explode('|', $questionArr['tags']);
        $member = Auth::user();

        $questionArr['currentUserCanAcceptSolution'] = 0;
        if ($questionArr['has_accepted_answer'] === 0 && $question->member == $member) {
            $questionArr['currentUserCanAcceptSolution'] = 1;
        }

        $questionsView = new QuestionsView(
            $questionArr
        );

        return response()->json([
                'content' => $questionsView,
                'server_date' => strtotime('now')
            ]
        );
    }


    /**
     * Create Question, it does accept extra parameters courseTag and lessonId
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function createQuestion(Request $request)
    {
        if (is_null($request->get('text'))) {
            return response()->json('Validation error', 400);
        }
        if (is_null($request->get('description'))) {
            return response()->json('Validation error', 400);
        }

        $filterByAudience = '';
        if (!is_null($request->route('audience'))) {

            $response = $this->authService->validateRequestOrigin(
                $request->route('audience')
            );

            $filterByAudience = $request->route('audience');

            if($response !== true){
                return response()->json('No rights to see this content', 403);
            }
        }


        //get the details of the user after login
        /**
         * @var $member Member
         */
        $member = $this->authService->getAuthenticatedUser();
        if ($member == null) {
            return response()->json('Authentication error', 403);
        }

        $text = htmlspecialchars($request->get('text'), ENT_NOQUOTES, 'UTF-8', false);
        $description = htmlspecialchars($request->get('description'), ENT_NOQUOTES, 'UTF-8', false);

        //if courseTag present add to the question detail
        if (!is_null($request->get('courseTag')) && !empty($request->get('courseTag'))) {
            $courseTag = htmlspecialchars($request->get('courseTag'), ENT_NOQUOTES, 'UTF-8', false);

            if(!empty($description)) {
                $description = $description . ' ' . $courseTag;
            } else {
                $text = $text . ' ' . $courseTag;
            }
        }

        preg_match_all("/#([a-zA-Z0-9\-]+)/", $text . ' ' . $description, $tagsRegArr);

        //Extract Tags out of the given text and description (and create the tags if they do not exists in db) and
        //attach them to the newly created question
        $countNonUniqueTags = 0;
        $tagsArr = [];
        if (isset($tagsRegArr[1]) && !empty($tagsRegArr[1])) {
            if (is_array($tagsRegArr)) {
                $countNonUniqueTags = count($tagsRegArr[1]);
            }

            $tagsRegArr[1] = array_map('strtolower', $tagsRegArr[1]);
            $tagsArrOrig = array_unique($tagsRegArr[1]);

            if ($countNonUniqueTags >= 20) {
                return response()->json('Validation error: maximum 19 tags permitted.', 400);
            }

            $tags = Tag::whereIn('name', $tagsArrOrig)->get();
            $tagsArr = array_flip($tagsArrOrig);
        }

        $question = new Question();
        $question->member()->associate($member);
        $question->audience = $filterByAudience;

        // if lessonId present add to the question
        if (!is_null($request->get('lessonId'))) {
            $question->lesson_id = $request->get('lessonId');
        }

        if(isset($tagsArrOrig) && !empty($tagsArrOrig)){
            $question->tags = implode('|', $tagsArrOrig);
        }

        if(isset($filterByAudience)){
            $question->audience = $filterByAudience;
        }

        $question->text = $text;
        $question->detail = $description;
        $question->save();

        if (isset($tags)) {
            foreach ($tags as $tag) {
                if (isset($tagsArr[$tag->name])) {
                    $question->tags()->save($tag);
                    unset($tagsArr[$tag->name]);
                }
            }

            $tagsToPersist = [];
            foreach ($tagsArr as $key => $tag) {
                $tag = new Tag();
                $tag->name = $key;
                $tag->save();

                $tagsToPersist[] = $tag;
            }

            if (!empty($tagsToPersist)) {
                $question->tags()->saveMany($tagsToPersist);
            }
        }

        return response()->make(
            ['content' => ['id' => $question->id]],
            201
        );
    }


    /**
     * Return dedicated question types.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function getQuestions(Request $request)
    {
        Log::debug("Request getQuestions started " . microtime());

        $filterByAudience = '';
        if (!is_null($request->route('audience'))) {

            $response = $this->authService->validateRequestOrigin(
                $request->route('audience')
            );

            $filterByAudience = $request->route('audience');

            if($response !== true){
                return response()->json('No rights to see this content', 403);
            }
        }

        $questions = [];
        if (!is_null($request->get('mode'))) {
            if ($request->get('mode') === 'unanswered') {
                $questions = Question::with(['member'])
                    ->where('audience', $filterByAudience)
                    ->whereNull('latest_activity_date')
                    ->orderBy('created_at', 'asc')
                    ->limit(self::QUESTIONSPERPAGE)
                    ->get();
            }
            elseif ($request->get('mode') === 'active') {

                $questions = Question::with(['member'])
                    ->where('audience', $filterByAudience)
                    ->whereNotNull('latest_activity_date')
                    ->orderBy('latest_activity_date', 'desc')
                    ->limit(self::QUESTIONSPERPAGE)
                    ->get();
            }
            elseif ($request->get('mode') === 'user') {
                if(!is_null($request->get('username')) && !empty($request->get('username'))) {

                    $questions = Question::with(['member'])
                        ->where('audience', $filterByAudience)
                        ->whereHas('member', function ($query) use ($request) {
                            $query->where('username', $request->get('username'));
                        })
                        ->orderBy('forum_questions.created_at', 'desc')
                        ->limit(self::QUESTIONSPERPAGE)
                        ->get();
                }
            }
        }
        else {
            $questions = Question::with('member')
                ->where('audience', $filterByAudience)
                ->orderBy('created_at', 'asc')
                ->limit(self::QUESTIONSPERPAGE)
                ->get();
        }
        
        $response = [];
        foreach ($questions as $question) {
            if(method_exists($question, 'toArray')) {
                $questionArr = $question->toArray();
            } else {
                $questionArr = (array) $question;
            }

            $questionArr['user'] = $question->member;
            $questionArr['objTags'] = explode('|', $questionArr['tags']);

            $response[] = new QuestionsView(
                $questionArr
            );
        }

        Log::debug("Request getQuestions ended " . microtime());

        return response()->json([
                'content' => $response,
                'server_date' => strtotime('now')
            ]
        );
    }



    /**
     * Delete a specific question and all answers and votes beloging to it.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function deleteQuestion(Request $request)
    {
        if (is_null($request->route('question_id'))) {
           return response()->json('Validation error', 400);
        }

        $filterByAudience = '';
        if (!is_null($request->route('audience'))) {

            $response = $this->authService->validateRequestOrigin(
                $request->route('audience')
            );

            $filterByAudience = $request->route('audience');

            if($response !== true){
                return response()->json('No rights to see this content', 403);
            }
        }

        //get the details of the user after login
        /**
         * @var $member Member
         */
        $member = $this->authService->getAuthenticatedUser();
        if ($member == null) {
            return response()->json('Authentication error', 403);
        }

        //check that the call user is a mentor
        if ($member->is_mentor !== 1) {
            return response()->json('Validation error, not a mentor', 400);
        }

        Log::debug("Request question started " . $request->route('question_id'));

        //get the question to remove
        $question = Question::where('id', $request->route('question_id'))
                    ->where('audience', $filterByAudience)
                    ->first();

        if (empty($question)) {
           return response()->make('Not found question', 404);
        }

        $answers = Answer::with(['answerVotes'])->where('question_id', $request->route('question_id'))
            ->get();

        foreach($answers as $answer) {
            $ansVotes = $answer->answerVotes;

            if (!empty($ansVotes->toArray())) {
                foreach($ansVotes as $ansVote){
                    $ansVote->delete();
                }
            }
            $answer->delete();
        }

        $question->delete();

        return response()->make(
            '',
            200
        );
    }


    /**
     * Edit Question, it does accept extra parameters courseTag and lessonId
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function editQuestion(Request $request)
    {
        if (is_null($request->route('question_id'))) {
            return response()->json('Validation error', 400);
        }
        if (is_null($request->get('text'))) {
            return response()->json('Validation error', 400);
        }
        if (is_null($request->get('description'))) {
            return response()->json('Validation error', 400);
        }

        $filterByAudience = '';
        if (!is_null($request->route('audience'))) {

            $response = $this->authService->validateRequestOrigin(
                $request->route('audience')
            );

            $filterByAudience = $request->route('audience');

            if($response !== true){
                return response()->json('No rights to see this content', 403);
            }
        }

        //get the details of the user after login
        /**
         * @var $member Member
         */
        $member = $this->authService->getAuthenticatedUser();
        if ($member == null) {
            return response()->json('Authentication error', 403);
        }

        //check that the call user is a mentor
        if ($member->is_mentor !== 1) {
            return response()->json('Validation error, not a mentor', 400);
        }

        //get the question to edit
        $question = Question::where('id', $request->route('question_id'))
                        ->where('audience', $filterByAudience)
                        ->first();

        if (empty($question)) {
            return response()->make('Not found question', 404);
        }

        $text = htmlspecialchars($request->get('text'), ENT_NOQUOTES, 'UTF-8', false);
        $description = htmlspecialchars($request->get('description'), ENT_NOQUOTES, 'UTF-8', false);

        //if courseTag present add to the question detail
        if (!is_null($request->get('courseTag')) && !empty($request->get('courseTag'))) {
            $courseTag = htmlspecialchars($request->get('courseTag'), ENT_NOQUOTES, 'UTF-8', false);

            if(!empty($description)) {
                $description = $description . ' ' . $courseTag;
            } else {
                $text = $text . ' ' . $courseTag;
            }
        }

        preg_match_all("/#([a-zA-Z0-9\-]+)/", $text . ' ' . $description, $tagsRegArr);

        //Extract Tags out of the given text and description (and create the tags if they do not exists in db) and
        //attach them to the newly created question
        $countNonUniqueTags = 0;
        $tagsArr = [];
        if (isset($tagsRegArr[1]) && !empty($tagsRegArr[1])) {
            if (is_array($tagsRegArr)) {
                $countNonUniqueTags = count($tagsRegArr[1]);
            }

            $tagsRegArr[1] = array_map('strtolower', $tagsRegArr[1]);
            $tagsArrOrig = array_unique($tagsRegArr[1]);

            if ($countNonUniqueTags >= 20) {
                return response()->json('Validation error: maximum 19 tags permitted.', 400);
            }

            $tags = Tag::whereIn('name', $tagsArrOrig)->get();
            $tagsArr = array_flip($tagsArrOrig);
        }


        // if lessonId present, add it to the question
        if (!is_null($request->get('lessonId'))) {
            $question->lesson_id = $request->get('lessonId');
        }

        if(isset($tagsArrOrig) && !empty($tagsArrOrig)){
            $question->tags = implode('|', $tagsArrOrig);
        }

        $question->text = $text;
        $question->detail = $description;
        $question->save();

        if (isset($tags)) {
            foreach ($tags as $tag) {
                if (isset($tagsArr[$tag->name])) {
                    $question->tags()->save($tag);
                    unset($tagsArr[$tag->name]);
                }
            }

            $tagsToPersist = [];
            foreach ($tagsArr as $key => $tag) {
                $tag = new Tag();
                $tag->name = $key;
                $tag->save();

                $tagsToPersist[] = $tag;
            }

            if (!empty($tagsToPersist)) {
                $question->tags()->saveMany($tagsToPersist);
            }
        }

        return response()->make(
            '',
            200
        );
    }

    /**
     * Return all questions filtered by lesson Id.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function getQuestionsByLessonId(Request $request)
    {
        Log::debug("Request getQuestionsByLessonId started " . microtime());

        if (is_null($request->route('lesson_id'))) {
            return response()->json('Validation error', 400);
        }

        $filterByAudience = '';
        if (!is_null($request->route('audience'))) {

            $response = $this->authService->validateRequestOrigin(
                $request->route('audience')
            );

            $filterByAudience = $request->route('audience');

            if($response !== true){
                return response()->json('No rights to see this content', 403);
            }
        }

        $questions = Question::with(['member'])
            ->where('lesson_id', $request->route('lesson_id'))
            ->where('audience', $filterByAudience)
            ->orderBy('forum_questions.created_at', 'desc')
            ->limit(self::QUESTIONSPERPAGE)
            ->get();

        $response = [];
        foreach ($questions as $question) {
            if(method_exists($question, 'toArray')) {
                $questionArr = $question->toArray();
            } else {
                $questionArr = (array) $question;
            }

            $questionArr['user'] = $question->member;
            $questionArr['objTags'] = explode('|', $questionArr['tags']);

            $response[] = new QuestionsView(
                $questionArr
            );
        }

        Log::debug("Request getQuestionsByLessonId ended " . microtime());

        return response()->json([
                'content' => $response,
                'server_date' => strtotime('now')
            ]
        );
    }


    /**
     * Return all questions filtered by course tag.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function getQuestionsByCourseTag(Request $request)
    {
        Log::debug("Request getQuestionsByCourseTag started " . microtime());

        if (is_null($request->route('course_tag'))) {
            return response()->json('Validation error', 400);
        }

        $filterByAudience = '';
        if (!is_null($request->route('audience'))) {

            $response = $this->authService->validateRequestOrigin(
                $request->route('audience')
            );

            $filterByAudience = $request->route('audience');

            if($response !== true){
                return response()->json('No rights to see this content', 403);
            }
        }

        $questions = DB::table('forum_questions')
            ->select('forum_questions.*', 'members.usid', 'members.username', 'members.avatar', 'members.is_mentor')
            ->join('members', 'members.usid', '=', 'forum_questions.user_id')
            ->leftjoin('forum_questions_tags', 'forum_questions_tags.question_id', '=', 'forum_questions.id')
            ->join('forum_tags', 'forum_tags.id', '=', 'forum_questions_tags.tag_id')
            ->where('forum_tags.name', $request->route('course_tag'))
            ->where('audience', $filterByAudience)
            ->orderBy('forum_questions.created_at', 'desc')
            ->limit(self::QUESTIONSPERPAGE)
            ->get();

        $response = [];
        foreach ($questions as $question) {
            $questionArr = (array) $question;

            $questionArr['user'] = [
                'usid' => $questionArr['usid'],
                'username' => $questionArr['username'],
                'avatar' => $questionArr['avatar'],
                'is_mentor' => $questionArr['is_mentor']
            ];

            $questionArr['objTags'] = explode('|', $questionArr['tags']);

            $response[] = new QuestionsView(
                $questionArr
            );
        }

        Log::debug("Request getQuestionsByCourseTag ended " . microtime());

        return response()->json([
                'content' => $response,
                'server_date' => strtotime('now')
            ]
        );
    }
}