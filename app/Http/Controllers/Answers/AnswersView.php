<?php
/**
 * Created by PhpStorm.
 * User: marius
 * Date: 01/09/2016
 * Time: 11:35
 */

namespace App\Http\Controllers\Answers;

use App\Http\Controllers\Tags\TagsView;
use App\Http\Controllers\Profile\UserView;

class AnswersView
{
    public $id;
    public $text;
    public $tags;
    public $user;
    public $votes;
    public $accepted;
    public $created_at;
    public $updated_at;

    /**
     * AnswersView constructor.
     * @param $answerArr
     */
    public function __construct($answerArr)
    {
        $this->id = $answerArr['id'];
        $this->text = $answerArr['text'];
        $this->accepted = $answerArr['accepted'];
        $this->user = new UserView($answerArr['user']);
        $this->votes = $answerArr['voteView'];
        $this->created_at = strtotime($answerArr['created_at']);
        $this->updated_at = strtotime($answerArr['updated_at']);

        if(isset($answerArr['objTags'])) {
            foreach ($answerArr['objTags'] as $tag) {
                $tagArr = $tag->toArray();
                $this->tags[] = new TagsView($tagArr);
            }
        }
    }

}