<?php
/**
 * Created by PhpStorm.
 * User: marius
 * Date: 01/09/2016
 * Time: 11:35
 */

namespace App\Http\Controllers\Answers;

class VoteView
{
    public $count;
    public $hasCurrentUserVoted;

    /**
     * VoteView constructor.
     * @param $voteArr
     */
    public function __construct($voteArr)
    {
        $this->count = $voteArr['count'];
        $this->hasCurrentUserVoted = $voteArr['hasCurrentUserVoted'];
    }

}