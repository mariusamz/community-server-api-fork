<?php

namespace App\Http\Controllers\Tags;

use Illuminate\Foundation\Bus\DispatchesJobs;
use Illuminate\Routing\Controller as BaseController;
use Illuminate\Foundation\Validation\ValidatesRequests;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Amazing\Model\Tag;
use Illuminate\Support\Facades\Log;


class RestTagsController extends BaseController
{
    /**
     * Return all tags marked for auto suggestion.
     *
     * @param Request $request
     * @return \Illuminate\Http\JsonResponse
     */
    function getSuggestedTags(Request $request)
    {
        $tags = Tag::where('is_auto_suggested', '1')->get();

        $response = [];
        foreach ($tags as $tag) {
            $tagArr = $tag->toArray();

            $response[] = new TagsView($tagArr);
        }

        return response()->json(
            $response
        );
    }
}
