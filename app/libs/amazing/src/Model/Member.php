<?php

namespace Amazing\Model;

use Illuminate\Auth\Authenticatable;
use Illuminate\Contracts\Auth\Authenticatable as AuthenticatableContract;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;


class Member extends Model implements AuthenticatableContract
{
    use Authenticatable;

    protected $table = 'members';
    protected $guarded = ['id'];
    protected $hidden = ['password', 'password_reset'];
    protected $dates = ['dateadded', 'password_reset'];
    public $timestamps = false;

    protected $primaryKey = 'usid';

    //This is sometimes added to the model at runtime, and declaring it here as a member variable prevents
    // Eloquent from thinking it's a field in the database and throwing an error when saving/updating.
    public $unencryptedPassword = null;

    /**
     * Get the unique identifier for the user.
     *
     * @return mixed
     */
    public function getAuthIdentifier()
    {
        return $this[$this->primaryKey];
    }

    /**
     * Get the password for the user.
     *
     * @return string
     */
    public function getAuthPassword()
    {
        return $this->password;
    }

    /**
     * Get the token value for the "remember me" session.
     *
     * @return string
     */
    public function getRememberToken()
    {
        return null;
    }

    /**
     * Set the token value for the "remember me" session.
     *
     * @param  string  $value
     * @return void
     */
    public function setRememberToken($value)
    {

    }

    /**
     * Get the column name for the "remember me" token.
     *
     * @return string
     */
    public function getRememberTokenName()
    {
        return null;
    }

    /**
     * Get a display name string
     *
     * @return string
     */
    public function displayName()
    {
        return $this->firstname ?: $this->username ?: $this->lastname ?: '';
    }

    /**
     * Get the member's full name as a string
     *
     * @return string
     */
    public function fullName()
    {
        return sprintf('%s %s', $this->firstname, $this->lastname);
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function answers()
    {
        return $this->hasMany(Answer::class, 'usid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function question()
    {
        return $this->hasMany(Question::class, 'usid');
    }

    /**
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function answerVotes()
    {
        return $this->hasMany(AnswerVote::class, 'usid');
    }

    /**
     * Get the instructor profiles for this member
     *
     * @return \Illuminate\Database\Eloquent\Relations\HasMany
     */
    public function instructorProfiles()
    {
        return $this->hasMany(Instructor::class, 'usid', 'usid');
    }

}