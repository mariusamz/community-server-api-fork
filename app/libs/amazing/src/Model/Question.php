<?php

namespace Amazing\Model;

use Illuminate\Database\Eloquent\Model;

class Question extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'forum_questions';

    public $incrementing = true;

    public function member()
    {
        return $this->belongsTo(Member::class, 'user_id');
    }

    public function answers()
    {
        return $this->hasMany(Answer::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'forum_questions_tags');
    }
}
