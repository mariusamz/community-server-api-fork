<?php

namespace Amazing\Model;

use Illuminate\Database\Eloquent\Model;

class Answer extends Model
{
    /**
     * The table associated with the model.
     *
     * @var string
     */
    protected $table = 'forum_answers';

    public $incrementing = true;

    public function member()
    {
        return $this->belongsTo(Member::class, 'user_id');
    }

    public function question()
    {
        return $this->belongsTo(Question::class);
    }

    public function tags()
    {
        return $this->belongsToMany(Tag::class, 'forum_answers_tags');
    }

    public function answerVotes()
    {
        return $this->hasMany(AnswerVote::class, 'answer_id');
    }
}